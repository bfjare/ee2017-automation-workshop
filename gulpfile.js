// PLUGINS
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');  
var rename = require('gulp-rename');  
var uglify = require('gulp-uglify');  
var plumber = require('gulp-plumber');  
var runSequence = require('run-sequence');

//COMPILE SASS
gulp.task('sass', function(){
  return gulp.src('public/assets/sass/main.scss')
  	.pipe(plumber())
    .pipe(sass()) // Using gulp-sass
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

//WATCH FILES FOR CHANGES
gulp.task('watch',['browserSync', 'sass', 'scripts'], function(){
  gulp.watch('public/assets/sass/*.scss', ['sass']);
  gulp.watch('public/assets/js/*.js', ['scripts']);
  gulp.watch('system/user/templates/**/*.html', browserSync.reload); 
})

// BROWSER REFRESH
gulp.task('browserSync', function() {
	browserSync.init({
	    proxy:'192.168.33.10'
	})
})

// COMPILE JAVASCRIPT FILES
gulp.task('scripts', function() {
  gulp.src([
  		'public/assets/js/jquery.js',
  		'public/assets/bootstrap/js/bootstrap.js',
  		'public/assets/fancybox/fancybox.js',
  		'public/assets/js/cycle.js',
  		'public/assets/js/parallax.js',
  		'public/assets/js/iso.js',
  		'public/assets/js/sameHeight.js',
  		'public/assets/js/custom.js'
  	])
    .pipe(concat('global-min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/assets/js/min'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

//DEFAULT
gulp.task('default', function (callback) {
  runSequence(['sass','browserSync', 'watch'],
    callback
  )
})